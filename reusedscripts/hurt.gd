extends RayCast2D

export (int) var damage = 1
var target = Node2D


func _ready():
	pass 

# check for colision every frame, may be a better way of doing this?
func _process(_delta):
	if self.is_colliding():
		target = self.get_collider()
		if target.has_node("HealthSystem"):
			target.get_node("HealthSystem").hurt(damage) 
