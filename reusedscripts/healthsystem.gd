extends Node
export (int) var health = 5
export (int) var maxhealth = 5
export (bool) var player = false
export (bool) var invincibile = false
export (bool) var invincibileframes = false
export (float) var defaultinvincibilitytime = 1

var currentnode = null #place holder
var invincibilitytimer = 0
var flickercount = 0
var justhit = false

func hurt(amount):
	if invincibileframes == false:
		damage(amount)
	else:
		if justhit == false:
			if self.get_parent().get_name() == "Player":
				get_node("../Camera2D").updatehealth()
			justhit = true
			damage(amount)
	if health <= 0:
		if self.get_parent().get_name() == "Player":
			gameover()
		else:
			death()
func damage(amount):
	health -= amount
func death():
	self.get_parent().get_parent().remove_child(self.get_parent()) # This is retarded
func gameover():
	print("death")
	health = 5



# Called when the node enters the scene tree for the first time.
func _ready():
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if justhit == true:
		if invincibilitytimer >= defaultinvincibilitytime:
			self.get_node("../sprite").set_self_modulate(Color(1,1,1,1))
			justhit = false
			invincibilitytimer = 0
		else:
			invincibilitytimer += delta
			flickeranimation()
			
func flickeranimation():
	if flickercount == 1:
		self.get_node("../sprite").set_self_modulate(Color(1,1,1,.5))
	if flickercount >= 2:
		self.get_node("../sprite").set_self_modulate(Color(1,1,1,1))
		flickercount = 0
	flickercount += 1
	
