extends Camera2D

onready var healthsystem = get_node("../HealthSystem")
onready var healthcopy = get_node("Heart")

var currenthealth = 5
var maxhealth = 5
var lastmaxhealth = 0
var copy = Node2D
var currentnode = Node2D
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	updatehealth()

func updatehealth():
	currenthealth = healthsystem.get("health")
	maxhealth = healthsystem.get("maxhealth")
	if lastmaxhealth != maxhealth:
		lastmaxhealth = maxhealth
		for i in range(maxhealth):
			copy = healthcopy.duplicate()
			copy.set_name(str(i+1))
			copy.position.x = -155 + i*7
			copy.position.y = -86
			copy.set_visible(true)
			self.add_child(copy)
	for i in range(maxhealth):
		currentnode = get_node(str(i+1))
		if i+1 <= currenthealth:
			currentnode.set_frame(1)
		else:
			currentnode.set_frame(0)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
