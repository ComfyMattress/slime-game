extends KinematicBody2D

export (int) var horzspeed = 15 #how quickly the player accelerates each frame 
export (int) var maxspeed = 150 #max speed 
export (float) var friction = 2 #what x speed is / by when not moving
export (float) var airfriction = 1.1 # same concept but in the air
export (int) var jumpforce = 35 #
export (float) var gravity = 1.25 #prevents giant ass numbers this is probably unneeded but i think it makes the gameplay better
export (float) var maxair = .7 # hangtime

onready var sprite = get_node("sprite")
onready var jumptrace = get_node("jumptrace")
onready var leftwalltrace = get_node("leftwalltrace")
onready var rightwalltrace = get_node("rightwalltrace")
onready var toptrace = get_node("toptrace")

var airtime = 0
var jumping = false
var anim = "move"
var IsMoving = false
var velocity = Vector2()
var direction = "right"
var lastjump = 0

# everyframe call these
func _physics_process(delta):
	playermovement(delta)
	playeranimation()

#all playermovement logic
func playermovement(delta):
	#needs polish
	IsMoving = false
	# Inputs go here
	if Input.is_action_pressed('right'):
		velocity.x += horzspeed
		IsMoving = true
		direction = "right" # for animation
	if Input.is_action_pressed('left'):
		velocity.x -= horzspeed
		IsMoving = true
		direction = "left" # for animation
	#jump inputs
	if Input.is_action_pressed("jump"):
		jump(delta)
	if Input.is_action_just_released("jump"):
		jumping = false
		airtime = 0
	#friction
	if !IsMoving and jumptrace.is_colliding():
		velocity.x /= friction
	if !IsMoving and !jumptrace.is_colliding():
		velocity.x /= airfriction
	#this variable is a missonmer really prevents y from going to fast
	velocity.y /= gravity
	#gravity only when not touching ground
	if !jumptrace.is_colliding():
		velocity.y += 25
	#speed limits
	if velocity.x > maxspeed: 
		velocity.x = maxspeed
	if velocity.x < -maxspeed:
		velocity.x = -maxspeed
	
# warning-ignore:return_value_discarded
	move_and_slide(velocity) #tells the engine to move

# Manages player animation
func playeranimation():
	#todo jump animation system
	if IsMoving == false:
		sprite.set_animation("move")
		sprite.set_frame(0)
	elif IsMoving == true:
		sprite.set_animation("move")
		sprite.set_frame(1)
		
	if direction == "right":
		sprite.set_flip_h(false)
	if direction == "left":
		sprite.set_flip_h(true)	

#called when spacebar or w is pressed
func jump(delta):
	#sets everything up
	if jumptrace.is_colliding():
		jumping = true
		airtime = 0
	#allows the player to "bump" on a ceiling
	if toptrace.is_colliding():
		jumping = false
		airtime = 0
	#this gives it a parabula type feel
	if jumping == true:
		if airtime < maxair:
			airtime += delta
			velocity.y -= jumpforce * maxair/(airtime+(.25*maxair))
	#stops the player from going upwards
	if airtime >= maxair:
		jumping = false
		airtime = 0
